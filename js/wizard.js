
$.fn.stepsWizard  = function(options) {
	var steps = 0;
	var element = '#main';
	var form = '#sign_form';
	var tabClass = 'ul.wizard-steps li';
	var paneClass = '.tab-pane'
	var tabLength = $(tabClass).length-1;
	var previous = 'a.previous';
	var next = 'a.next';
	var obj = this;
	var myLanguage = {
		requiredField: '이 필드를 채워주세요.',
		badEmail: '이메일이 정확하지 않습니다.',
		badStrength: '영문/숫자/기호 중 2가지 이상 조합해야 합니다.'
	};
	$(tabClass).removeClass('active');
	$(tabClass+':first').addClass('active');
	$(paneClass).removeClass('active');
	$(paneClass+':first').addClass('active');
	$(previous).addClass('disabled');

	obj.next = function() {
		$(tabClass+':eq('+steps+')').removeClass('active');
		$(tabClass+':eq('+(steps+1)+')').addClass('active');

		$($(tabClass+':eq('+(steps)+') a').attr('href')).removeClass('active');
		$($(tabClass+':eq('+(steps+1)+') a').attr('href')).addClass('active');
		$(previous).removeClass('disabled');
		steps+=1;
		var percent = Math.floor(( steps / tabLength ) * 100);
		$(element).find('.progress-indicator').css({ 'width': percent + '%' });
		$(tabClass+':eq('+steps+')').prevAll().addClass('completed');
		$(tabClass+':eq('+steps+')').nextAll().removeClass('completed');
		if(steps == tabLength) {
			$(previous).css('display','none');
			$(next).css('display','none');
		}
	}
	obj.previous = function() {
		$(tabClass+':eq('+steps+')').removeClass('active');
		$(tabClass+':eq('+(steps-1)+')').addClass('active');

		$($(tabClass+':eq('+(steps)+') a').attr('href')).removeClass('active');
		$($(tabClass+':eq('+(steps-1)+') a').attr('href')).addClass('active');
		steps-=1;

		var percent = Math.floor(( steps / tabLength ) * 100);
		$(element).find('.progress-indicator').css({ 'width': percent + '%' });
		$(tabClass+':eq('+steps+')').prevAll().addClass('completed');
		$(tabClass+':eq('+steps+')').nextAll().removeClass('completed');
		if(steps == 0) {
			$(previous).addClass('disabled');
		}
	}
	$(next).click(function() {
		if(steps < tabLength) {	
			if($(form).isValid(myLanguage)){
				obj.next();
				$('body,html').animate({scrollTop: 0},0);
			}
		}
	});

	$(previous).click(function() {
		if(steps > 0) {
			obj.previous();
		}
	});
	$(tabClass+' a').click(function(event) {
		var step = $(tabClass).index($(this).parent());
		/* validation check */
		if(step == steps+1) {
			if($(form).isValid(myLanguage)){
				obj.next();
				$('body,html').animate({scrollTop: 0},0);
			}
		}
		else if(step == steps-1) {
			obj.previous();
		}
		return false;
	})
}

$.fn.stepsWizard.defaults = {
	onTabShow:        null
};